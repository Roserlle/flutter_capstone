import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:sqlite3/sqlite3.dart';

import '../portable_database.dart';

class User {
    static Map<String, String> login(String email, String password) {
        try {
            final Row user = db.select('SELECT * FROM users WHERE email = ?', [email]).first;

            if (new DBCrypt().checkpw(password, user['password'])) {
                return { 
                    'accessToken': JWT({ 'id': user['id'], 'designation': user['designation'] }).sign(SecretKey('FFUF-Dart')),
                    'designation': user['designation']
                };
            } else {
                return { 'error': 'Password given is incorrect.' };
            }
        } catch (e) {
            print(e.toString());
            return { 'error': 'User not found.' };
        }
    }

    static dynamic retrieve(String designation) {
        try {
            final ResultSet resolvers = db.select('SELECT id, email, designation FROM users WHERE designation = ?', [designation]);
            return (resolvers == null) ? List.empty() : resolvers.toList();
        } catch (e) {
            print(e.toString());
            return { 'error': 'User not found.' };
        }
    }
}