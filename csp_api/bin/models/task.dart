import 'dart:convert';
import 'dart:io';

import 'package:mime/mime.dart';
import 'package:sqlite3/sqlite3.dart';

import '../portable_database.dart';

class Task {
    static bool add(String title, String description, int assignedTo, int projectId) {
        try {
            final PreparedStatement statement = db.prepare('INSERT INTO tasks (title, description, status, assignedTo, projectId) VALUES (?, ?, ?, ?, ?)');

            statement.execute([title, description, 'Pending', assignedTo, projectId]);

            return true;
        } catch (e) {
            print(e.toString());
            return false;
        }
    }

    static dynamic retrieve(int projectId) {
        try {
            ResultSet tasks;

            tasks = db.select('SELECT * FROM tasks WHERE projectId = ?', [projectId]);

            if (tasks == null) {
                return List.empty();
            }

            return (tasks == null) ? List.empty() : tasks.toList();
        } catch (e) {
            print(e.toString());
            return { 'error': 'Task.retrieve() encountered an error.' };
        }
    }

    static bool updateStatus(int taskId, String status) {
        try {
            final PreparedStatement statement = db.prepare('UPDATE tasks SET status = ? WHERE id = ?');
            statement.execute([status, taskId]);

            return true;
        } catch (e) {
            print(e.toString());
            return false;
        }
    }

    static Future<Row> addImage(HttpRequest req) async {
        try {
            // Begin receiving bytes of requests from the client.
            List<int> requestBytes = [];
            await for (var data in req) { requestBytes.addAll(data); }

            // Convert received bytes of data into list of MimeMultipart elements.
            final boundary = req.headers.contentType.parameters['boundary'];
            final transformer = MimeMultipartTransformer(boundary);
            final bodyStream = Stream.fromIterable([requestBytes]);
            final fields = await transformer.bind(bodyStream).toList();

            // Retrieve index of form fields from the 'fields' list.
            final idFieldIdx = fields.indexWhere((field) => field.headers['content-disposition'].contains('id'));
            final imageFieldIdx = fields.indexWhere((field) => field.headers['content-disposition'].contains('image'));
            
            // Prepare constants for writing the file to the server.
            final contentDisposition = fields[imageFieldIdx].headers['content-disposition'];
            final filename = RegExp(r'filename="([^"]*)"').firstMatch(contentDisposition).group(1);
            final content = await fields[imageFieldIdx].toList();

            // Make sure that the target directory exists.
            if (!Directory('./uploads').existsSync()) {
                await Directory('./uploads').create();
            }

            // Use the list of integers from content[0] as bytes of data to be used for writing the file.
            await File('./uploads/$filename').writeAsBytes(content[0]);

            // Retrieve other form data by decoding the int data into string.
            // Parse string data into other data types as deemed fit.
            int id = int.parse(await utf8.decodeStream(fields[idFieldIdx]));
            
            // Save changes to the portable in-memory database.
            final PreparedStatement statement = db.prepare('UPDATE tasks SET imageLocation = ? WHERE id = ?');
            statement.execute(['$filename', id]);

            // Get the task.
            final Row task = db.select('SELECT * FROM tasks WHERE id = ?', [id]).first;

            return task;
        } catch (e) {
            print(e.toString());
            return null;
        }
    }
}