import 'dart:io';

import 'package:http_server/http_server.dart';

import './database_initializer.dart';
import './server_listener.dart';

void main() async {
    HttpServer apiServer = await HttpServer.bind('127.0.0.1', 4000);
    
    // Initialize tables and records within the portable database.
    initializePortableDatabase();

    // Listen to the events in the stream of the created web server.
    apiServer.listen(setServerListener);

    print('API server active at ${apiServer.address.host}:${apiServer.port}.');

    HttpServer staticServer = await HttpServer.bind('127.0.0.1', 4001);
    VirtualDirectory directory = VirtualDirectory('uploads')..allowDirectoryListing = true;

    print('Static server active at ${staticServer.address.host}:${staticServer.port}.');

    await for (HttpRequest request in staticServer) {
        await directory.serveRequest(request);
    }
}