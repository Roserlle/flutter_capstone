import 'dart:convert';
import 'dart:io';

import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';

import 'models/project.dart';
import 'models/task.dart';
import 'models/user.dart';

void setServerListener(HttpRequest req) async {
    String uri = req.uri.toString().split('?')[0];
    String reqContentType = req.headers.value(HttpHeaders.contentTypeHeader);
    dynamic body;

    int userId;
    String userDesignation;

    var result;

    // Attempt to extract the request body.
    if (reqContentType != null && !reqContentType.contains('multipart/form-data')) {
        try {
            body = jsonDecode(await utf8.decodeStream(req));
        } catch (e) {
            body = null;
        }
    }

    // Printing of body for debugging purposes.
    print(DateTime.now());
    print('Body: $body');

    // Extract userId if the request's authorization header has a value.
    if (req.headers.value(HttpHeaders.authorizationHeader) != null) {
        var token = req.headers.value(HttpHeaders.authorizationHeader).substring(7);
        var decodedToken = JWT.verify(token, SecretKey('FFUF-Dart')).payload;
        userId = decodedToken['id'];
        userDesignation = decodedToken['designation'];
    }

    // Set all responses to JSON type.
    req.response.headers.contentType = ContentType.json;

    // Set CORS policies in responses.
    req.response.headers.add('Access-Control-Allow-Origin', '*');
    req.response.headers.add('Access-Control-Allow-Headers', '*');
    req.response.headers.add('Access-Control-Allow-Methods', 'POST,GET,DELETE,PUT,OPTIONS');

    print('URI: $uri');
    print('Method: ${req.method}');

    if (uri == '/api/users/login' && req.method == 'POST') {
        result = User.login(body['email'], body['password']);
    } else if (uri == '/api/users/subcontractors' && req.method == 'GET') {
        result = User.retrieve('subcontractor');
    } else if (uri == '/api/users/assembly-teams' && req.method == 'GET') {
        result = User.retrieve('assembly-team');
    } 
    
    else if (uri == '/api/projects' && req.method == 'POST') {
        result = Project.add(body['name'], body['description'], userId);
    } else if (uri == '/api/projects' && req.method == 'GET') {
        result = Project.retrieve(userId, userDesignation);
    } else if (uri == '/api/projects/assign' && req.method == 'PUT') {
        result = Project.assign(body['assignedTo'], body['projectId']);
    } 
    
    else if (uri == '/api/tasks' && req.method == 'POST') {
        result = Task.add(body['title'], body['description'], body['assignedTo'], body['projectId']);
    } else if (uri == '/api/tasks' && req.method == 'GET') {
        result = Task.retrieve(int.parse(req.uri.queryParameters['projectId']));
    } else if (uri == '/api/tasks/update-status' && req.method == 'PUT') {
        result = Task.updateStatus(body['taskId'], body['status']);
    } else if (uri == '/api/tasks/image' && req.method == 'POST') {
        result = await Task.addImage(req);
    }
    
    else if (req.method == 'OPTIONS') {
        result = 'The ${req.method} request has been sent to ${uri}.';
    } else {
        result = { 'error': 'Cannot ${req.method} ${uri}.' };
    }

    // Printing of result for debugging purposes.
    print('Result: $result\n');

    req.response.write(jsonEncode(result));
    req.response.close();
}