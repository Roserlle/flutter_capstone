# Flutter Capstone App

# Setup Guide

## Project Cloning

### Clone a repository using the command line

You can use Git from the command line, or any client you like to clone your Git repository. These instructions show you how to clone your repository using Git from the terminal.

1. From the repository, click + in the global sidebar and select Clone this repository under Get to work.
2. Copy the clone command (either the SSH format or the HTTPS). If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.
3. From a terminal window, change to the local directory where you want to clone your repository.
4. Paste the command you copied from Bitbucket, for example:

### Clone over HTTPS

$ `git clone https://username@bitbucket.org/teamsinspace/documentation-tests.git`

### Clone over SSH

$ `git clone ssh://git@bitbucket.org:teamsinspace/documentation-tests.git`

5. Press Enter to create your local clone.

```
$ git clone git@bitbucket.org:Roserlle/flutter_capstone.git
> Cloning into `Spoon-Knife`...
> remote: Counting objects: 10, done.
> remote: Compressing objects: 100% (8/8), done.
> remove: Total 10 (delta 1), reused 10 (delta 1)
> Unpacking objects: 100% (10/10), done.
```
NOTE: If the clone was successful, a new sub-directory appears on your local drive in the directory where you cloned your repository. This directory has the same name as the Bitbucket repository that you cloned. The clone contains the files and metadata that Git requires to maintain the changes you make to the source files.

### Run the Flutter App
1. Go back to your terminal, change directory and run your API.

$ `cd ..`

$ `cd csp_api`

$ `dart run bin/main.dart`

`API server active at 127.0.0.1:4000.`

`Static server active at 127.0.0.1:4001.`

2. Go back to your terminal, change directory and open your clone project.

$ `cd ..`

$ `cd flutter_capstone`

3. Run the flutter app

$ `flutter run`

---

## Package Installation

Flutter supports using shared packages contributed by other developers to the Flutter and Dart ecosystems. This allows quickly building an app without having to develop everything from scratch.

Packages are published to [pub.dev](https://pub.dev/).

### Adding a package dependency to an app

1. Open the pubspec.yaml file located inside the app folder, and add packages under dependencies.
2. Install it. 
    - From the terminal: Run `flutter pub get`.
    - From Android Studio/IntelliJ: Click Packages get in the action ribbon at the top of pubspec.yaml. 
    - From VS Code: Click Get Packages located on right side of the action ribbon at the top of pubspec.yaml.
3. Import it. Add a corresponding import statement in the Dart code.
4. Stop and restart the app, if necessary.

---

# Test User Credentials
| Email  | Password |
| ------------- | ------------- |
| contractor@gmail.com  | contractor  |
| subcontractor@gmail.com  | subcontractor  |
| assembly-team@gmail.com  | assembly-team  |

---

# Version

Flutter 2.2.3

---

# Packages

[http](https://pub.dev/packages/http): ^0.13.3 

A composable, Future-based library for making HTTP requests.

[provider](https://pub.dev/packages/provider) 5.0.0 

A wrapper around InheritedWidget to make them easier to use and more reusable.

[shared_preferences](https://pub.dev/packages/shared_preferences): ^2.0.6 

Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.).

[image_picker](https://pub.dev/packages/image_picker/changelog): ^0.8.0+3 

A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.

[flutter_dotenv](https://pub.dev/packages/flutter_dotenv): ^5.0.0

Load configuration at runtime from a .env file which can be used throughout the application.

[rive](https://pub.dev/packages/rive): ^0.7.22 

Rive is a real-time interactive design and animation tool.

---

# Features

## Login Page

- Use the email address, rather than a username.
- Let users see their password (if they want to) to avoid mistyping it. Just click the eye toggle icon.
- Warn users if the data input in the form is incorrect or incomplete.

## Project List Screen

### Contractor

- All Projects added will be displayed.
- Contractor can add project.
- Contractor can assign the project to the subcontractor.
- Contractor is able to review finished project tasks.

### Subcontractor

- Assigned projects to Subcontractor will be displayed.
- Subcontractor can add task and assign it to Assembly team.
- All project tasks will be displayed.

### Assembly Team

- The projects and assigned tasks to Assembly will be displayed.
- All project tasks will be displayed.
- They can tag task if ongoing or complete.

## About the Developer

- Short introduction about the Developer
- Work Experience
- Skills
- Interests
- Contact Information

---
