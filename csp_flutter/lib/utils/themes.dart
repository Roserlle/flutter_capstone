import 'package:flutter/material.dart';

var backgroundColor = Color.fromRGBO(205, 23, 25, 1);
var colorWhite = Colors.white;
var colorBlack = Colors.black;
var drawerColor = Color.fromRGBO(20, 45, 68, 1);
var width = double.infinity;

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: colorWhite,
    side: BorderSide(width: 1.0, color: Colors.black),
    onPrimary: colorBlack// text color
);

var themeData = ThemeData(
    primaryColor: backgroundColor,
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
            primary: backgroundColor, 
            onPrimary: colorWhite
        )
    )
);

var expansionStyle = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 25,
    color: drawerColor,
);

var boxDecoration = BoxDecoration(
    border: Border.all(color: Color.fromRGBO(20, 45, 68, 1)),
    borderRadius: BorderRadius.all(Radius.circular(10))
);

var listTileStyle = TextStyle(
    fontSize: 18,
    color: Color.fromRGBO(20, 45, 68, 1)
);

var textUnderline = TextStyle(
    fontSize: 12,
    color: drawerColor,
    decoration: TextDecoration.underline
);

var contactTitle = TextStyle(
    fontSize: 15,
    color: drawerColor,
    fontWeight: FontWeight.bold
);



