import 'package:flutter/material.dart';
import '/utils/themes.dart';

class MyBullet extends StatelessWidget{
    @override
    Widget build(BuildContext context) {
        return Container(
            height: 15,
            width: 15,
            decoration: BoxDecoration(
                color: Color.fromRGBO(20, 45, 68, 1),
                shape: BoxShape.circle,
            ),
        );
    }
}

class ClippingClass extends CustomClipper<Path> {  
    @override  
    Path getClip(Size size) {  
        var path = Path();

        path.lineTo(0.0, size.height - 80);
        
        path.quadraticBezierTo(  
            size.width / 4,  
            size.height,  
            size.width / 2,  
            size.height,  
        );

        path.quadraticBezierTo(  
            size.width - (size.width / 4),  
            size.height,  
            size.width,  
            size.height - 80,  
        );

        path.lineTo(size.width, 0.0);  
        path.close();
        
        return path;  
    }  
    
    @override  
    bool shouldReclip(CustomClipper<Path> oldClipper) => false;  
}  

