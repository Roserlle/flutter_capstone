import 'package:csp_flutter/utils/themes.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/providers/user_provider.dart';
import '/screens/about_screen.dart';
import '/screens/login_screen.dart';
import '/screens/project_list_screen.dart';
import '/utils/themes.dart';


Future<void> main() async {
    // Initial checks for user's access token from SharedPreferences.
    // Determine initial route of app depending on existence of user's access token.

    await dotenv.load(fileName: '.env');    
    WidgetsFlutterBinding.ensureInitialized();
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken');
    String? designation = prefs.getString('designation');
    String initialRoute = (accessToken != null) ? '/project-list' : '/';

    runApp(App(initialRoute, accessToken, designation));
}

class App extends StatelessWidget {
    final String _initialRoute;
    final String? _accessToken;
    final String? _designation;

    App(this._initialRoute, this._accessToken, this._designation);

    @override
    Widget build(BuildContext context) {
        // This is to make the UserProvider available app-wide.

        return ChangeNotifierProvider(
            create: (BuildContext context) => UserProvider(_accessToken, _designation),
            child: MaterialApp(
                theme: themeData,
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/project-list': (context) => ProjectListScreen(),
                    '/about': (context) => AboutScreen()
                }
            )
        );
    }   
}