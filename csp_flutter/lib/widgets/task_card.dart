import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';
import '/utils/api.dart';
import '/utils/themes.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}

class _TaskCard extends State<TaskCard> {    
    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(widget._task.title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                            Text(widget._task.description)
                        ]
                    )
                ),
                Chip(label: Text(widget._task.status!))
            ]
        );

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Start'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId: widget._task.id, status: 'Ongoing').then((value) => widget._reloadTasks())
                    .catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Finish'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId: widget._task.id, status: 'Completed').then((value) => widget._reloadTasks())
                    .catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Accept'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId: widget._task.id, status: 'Accepted').then((value) => widget._reloadTasks())
                    .catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Reject'),
                style: btnDefaultTheme,
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId: widget._task.id, status: 'Rejected').then((value) => widget._reloadTasks())
                    .catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text(
                    'Detail',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline
                    )
                ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),   
                (designation == 'assembly-team' && widget._task.status == 'Pending' ? btnStart : Container()),
                (designation == 'assembly-team' && widget._task.status == 'Ongoing' ? btnFinish : Container()),
                (designation == 'contractor' && (widget._task.status == 'Completed' || widget._task.status == 'Rejected') ? btnAccept : Container()),
                (designation == 'contractor' && (widget._task.status == 'Completed' || widget._task.status == 'Accepted') ? btnReject : Container()),
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}