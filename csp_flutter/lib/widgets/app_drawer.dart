import 'package:csp_flutter/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/user_provider.dart';
import '/utils/themes.dart';

class AppDrawer extends StatelessWidget {
    
    @override
    Widget build(BuildContext context) {
        return Drawer(
            child: Container(
                width: width,
                color: drawerColor,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    SizedBox(height: 36.0),
                                    Image.asset(
                                        'assets/ffuf-logo.png',
                                        color: Colors.white,
                                        width: 100
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(top: 16.0),
                                        child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Text('FFUF Project Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                Text('Made by FFUF Internal Dev Team', style: TextStyle(color: Colors.white)),
                                                Divider(color: Colors.white30)
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 3),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    ListTile(
                                        title: RichText(text: TextSpan(
                                            style: TextStyle(fontSize: 16.0, color: Colors.white),
                                            children: [
                                                WidgetSpan(child: Icon(Icons.account_circle, size: 17, color: Colors.white)),
                                                TextSpan(text: ' About Me'),
                                            ],
                                        )),
                                        onTap: () async {
                                            Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                            Navigator.pushNamed(context, '/about');
                                        }
                                    ),
                                ],
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                                    }
                                )
                            ]
                        )
                    ]
                )
            )
        );
    }
}