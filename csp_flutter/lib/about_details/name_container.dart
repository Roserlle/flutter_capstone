import 'package:flutter/material.dart';

import '/utils/themes.dart';

class NameContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.only(top: 25),
            child: Column(
                children: [
                    Text(
                        'Hi There!',
                        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 50, color: Colors.orange),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                        child: Text(
                            'My name is Roselle Burlasa',
                            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20, color: drawerColor),
                        ),
                    ),
                ],
            ),
        );
    }
}