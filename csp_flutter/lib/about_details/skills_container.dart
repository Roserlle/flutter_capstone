import 'package:flutter/material.dart';

import '/utils/list_function.dart';
import '/utils/themes.dart';

class SkillsContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(20, 5, 20, 7),
            decoration: boxDecoration,
            child: ExpansionTile(
                leading: Icon(Icons.model_training, color: Colors.black),
                title: Text(
                    'Skills',
                    style: expansionStyle
                ),
                children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: generateSkillList()
                        )
                    )
                ],
            )
        );
    }
}