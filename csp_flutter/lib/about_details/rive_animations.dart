import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class RiveAnimations extends StatelessWidget {
    const RiveAnimations({ Key? key }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Column(
            children: [
                Container(
                    margin: EdgeInsets.only(top: 50),
                    width: double.infinity,
                    height: 400,
                    child:  RiveAnimation.asset(
                        'assets/facial_expression_demo.riv',
                    ),
                ),
                Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                        'Hi There!',
                        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 50, color: Colors.orange),
                    ),
                ),
                TweenAnimationBuilder(
                    curve: Curves.bounceInOut,
                    tween: Tween<double>(begin: 30, end: 100), 
                    duration: Duration(seconds: 2), 
                    builder: (BuildContext context, dynamic value, Widget? child){
                        return Icon(
                            Icons.arrow_downward,
                            color: Colors.white,
                            size: value,
                        );
                    }
                )
            ],
        );
    }
}